'use strict';
var request = require('request');
var getSlug = require('speakingurl');
var qs = require('querystring');

var APIv1URL = 'https://bitbucket.org/api/1.0';
var APIv2URL = 'https://bitbucket.org/api/2.0';

var Bitbucket = module.exports = function Bitbucket() {

};

Bitbucket.ERROR_UNAUTHORIZED = 'ERROR_UNAUTHORIZED';
Bitbucket.ERROR_INVALID_OWNER = 'ERROR_INVALID_OWNER';
Bitbucket.ERROR_UNKNOWN = 'ERROR_UNKNOWN';

Bitbucket.createClient = function createClient(options) {
  var instance = new Bitbucket();
  instance.setCredentials(options);
  return instance;
};

Bitbucket.slugify = function slugify(name) {
  // TODO dot(.) support
  return getSlug(name, {
    spearator: '-',
    lang: 'en',
    maintainCase: false,
    uric: false,
    uricNoSlash: false,
    mark: false
  });
};

Bitbucket.prototype.setCredentials = function setCredentials(options) {
  this.username = options.username;
  this.password = options.password;
};

/**
* repository
* {
*   owner
*   slug:
*   scm:
*   name:
*   is_private:
*   description:
*   forking_policy:
*   has_issues:
*   has_wiki:
*  }
*/
Bitbucket.prototype.createRepository = function createRepository(repository, cb) {
  var options = {};
  options.auth = this.getRequestAuth();
  options.json = true;
  options.body = qs.stringify({
    scm: repository.scm,
    name: repository.name,
    is_private: repository.is_private,
    description: repository.description,
    forking_policy: repository.forking_policy,
    has_issues: repository.has_issues,
    has_wiki: repository.has_wiki
  });

  request.post(APIv2URL + '/repositories/' + repository.owner + '/' + repository.slug, options, function (err, resp, body) {
    this.parseReponse(err, resp, body, cb);
  }.bind(this));
};

Bitbucket.prototype.getRepository = function getRepository(repository, cb) {
  var options = {};
  options.auth = this.getRequestAuth();
  request.get(APIv2URL + '/repositories/' + repository.owner + '/' + repository.slug, options, function (err, resp, body) {
    this.parseReponse(err, resp, body, cb);
  }.bind(this));
};

Bitbucket.prototype.parseReponse = function parse(err, resp, body, cb) {
  if (!err && (resp.statusCode >= 200 && resp.statusCode < 300)) {
    if (body && typeof body === 'string') {
      body = JSON.parse(body);
    }
    cb(err, body);
  } else {
    switch (resp.statusCode) {
      case 401:
        err = Bitbucket.ERROR_UNAUTHORIZED;
        break;
      default:
        var errorFields;
        if (body.error && (errorFields = body.error.fields) && errorFields.owner) {
          err = Bitbucket.ERROR_INVALID_OWNER;
        } else {
          throw new Error(JSON.stringify(body));
          // err = Bitbucket.ERROR_UNKNOWN;
        }
        break;
    }
    cb(err, {});
  }
};

Bitbucket.prototype.getRequestAuth = function getRequestAuth() {
  return {
    user: this.username,
    pass: this.password,
    sendImmediately: true
  };
};

Bitbucket.prototype.deleteRepository = function deleteRepository(repository, cb) {
  var options = {
    auth: this.getRequestAuth()
  };
  request.del(APIv2URL + '/repositories/' + repository.owner + '/' + repository.slug, options, function (err, resp, body) {
    this.parseReponse(err, resp, body, cb);
  }.bind(this));
};

Bitbucket.prototype.getUserAdminPrivilegesTeams = function getUserAdminPrivilegesTeams(cb) {
  var options = {
    auth: this.getRequestAuth(),
  };

  var ret = [];
  var _this = this;
  function _getUserAdminPrivilegesTeams(err, body) {
    if (err) {
      cb(err, []);
    } else {
      var userPrivileges = body.teams;
      userPrivileges[_this.username] = 'admin'; // for convenience

      for (var privilegesItem in userPrivileges) {
        ret.push(privilegesItem);
      }
      cb(err, ret);
    }
  }

  request.get(APIv1URL + '/user/privileges/', options, function (err, resp, body) {
    this.parseReponse(err, resp, body, _getUserAdminPrivilegesTeams);
  }.bind(this));
};
